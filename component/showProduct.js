import { Text,StyleSheet, View,Dimensions, Image, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";
import { useState, useEffect } from "react";
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'UserDatabase.db' });
const width = Dimensions.get('screen').width;
const ShowProduct = () => {

    const navigation = useNavigation();
    const [isModalVisible, setIsModalVisible] = useState(false);
    const [productItems, setProductItems] = useState([]);

    useEffect(() => {
        db.transaction((tx) => {
            tx.executeSql(
                'SELECT * FROM product_table2',
                [],
                (tx, results) => {
                    var temp = [];
                    for (let i = 0; i < results.rows.length; ++i) {
                        console.log("item:", results.rows.item(i))
                        temp.push(results.rows.item(i));
                    }
                    setProductItems(temp);
                }
            );
        });

    }, []);

    console.log("productItems", productItems)

    return (
        <>
             <View style={styles.mainView}>
                {productItems.length == 0 &&(
                <View>
                    <Text style={{fontSize:18}}>No Product Available</Text>
                </View>)}
                <FlatList
                    data={productItems}
                    renderItem={({item}) => {
                    return (
                        <View style={styles.card} key={item.id}>
                            <Image
                                resizeMode='cover'
                                style={styles.image}
                                source={{ uri: item.imageUri }}
                            />
                            <Text style={styles.cardText}>{"Name  : "+ item.name}</Text>
                            <Text style={styles.cardText}>{"Price : Rs " + item.price}</Text>
                            <Text style={styles.cardText}>{"Booked date : " + item.date_column}</Text>
                            <Text style={styles.cardText}>{"Description : "+ item.description}</Text>
                        </View>
                    )
                    }}
                />
            </View> 
        </>
    )
}

export default ShowProduct;

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "grey"
    },
    card: {
        backgroundColor: "white",
        width: width-40,
        height: 250,
        borderRadius: 10,
        marginTop: 20,
    },
    image:{
        height: "60%", 
        width: "100%",
        backgroundColor:"white",
        borderTopRightRadius :10,
        borderTopLeftRadius :10
    },
    cardText:{
        fontSize:14,
        color :"black",
        marginLeft:5,
        marginRight :5,
        width : width-50
    },
})