import { View, Text, TouchableOpacity, Alert, StyleSheet, Image, FlatList } from "react-native";
import { useState, useEffect } from "react";
import { Agenda } from 'react-native-calendars';
import { useNavigation } from "@react-navigation/native";
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'UserDatabase.db' });


const Calender = () => {

    const navigation = useNavigation();
    const [items, setItems] = useState({});
    const [productItems, setProductItems] = useState([]);

    useEffect(() => {
        db.transaction((tx) => {
            tx.executeSql(
                'SELECT * FROM product_table2',
                [],
                (tx, results) => {
                    var temp = [];
                    for (let i = 0; i < results.rows.length; ++i) {
                        console.log("item:", results.rows.item(i))
                        temp.push(results.rows.item(i));
                    }
                    setItems(temp);
                }
            );
        });

    }, []);

    const timeToString = (time) => {
        const date = new Date(time);
        return date.toISOString().split('T')[0];
    }

    const loadItems = (day) => {
        const items = {};

        // setTimeout(() => {
        //     for (let i = -15; i < 85; i++) {
        //         const time = day.timestamp + i * 24 * 60 * 60 * 1000;
        //         const strTime = timeToString(time);

        //         if (!items[strTime]) {
        //             items[strTime] = [];

        //             const numItems = Math.floor(Math.random() * 3 + 1);
        //             for (let j = 0; j < numItems; j++) {
        //                 items[strTime].push({
        //                     name: strTime,
        //                     height: Math.max(50, Math.floor(Math.random() * 150)),
        //                     day: strTime
        //                 });
        //             }
        //         }
        //     }

        //     const newItems = {};
        //     Object.keys(items).forEach(key => {
        //         newItems[key] = items[key];
        //     });
        //     setItems(newItems);
        // }, 1000);
    }


    const renderItem = () => {
        // console.log("product", productItems)
        return (
            // <TouchableOpacity
            //     style={{ backgroundColor: "white", width: "90%",margin: 10,}}
            //     onPress={() => console.log(reservation)}
            // >
            //     <Text>{reservation.name}</Text>
            // </TouchableOpacity>
            // {
            //     productItems.map((item) => {

            //         <View style={styles.card} >
            //             {/* <Image
            //             resizeMode='cover'
            //             style={styles.image}
            //             // source={{ uri:  }}
            //         /> */}
            //             <Text style={styles.cardText}>{"Name  : " + {item}}</Text>
            //             <Text style={styles.cardText}>{"Price : Rs "}</Text>
            //             <Text style={styles.cardText}>{"Description : "}</Text>
            //         </View>
            //     }
            //     )
            // }
            <View style={styles.card} >
                {/* <Image
                         resizeMode='cover'
                        style={styles.image}
                         // source={{ uri:  }}
                    /> */}
                {/* <Text style={styles.cardText}>{"Name  : " }</Text>
                        <Text style={styles.cardText}>{"Price : Rs "}</Text>
                        <Text style={styles.cardText}>{"Description : "}</Text> */}

                <FlatList
                    data={items}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.card} key={item.id}>
                                {/* <Image
                                    resizeMode='cover'
                                    style={styles.image}
                                    source={{ uri: item.imageUri }}
                                /> */}
                                <Text style={styles.cardText}>{"Name  : " + item.name}</Text>
                                <Text style={styles.cardText}>{"Price : Rs " + item.price}</Text>
                                <Text style={styles.cardText}>{"Booked date : " + item.date_column}</Text>
                                <Text style={styles.cardText}>{"Description : " + item.description}</Text>
                            </View>
                        )
                    }}
                />
            </View>

        );
    }

    const renderEmptyDate = () => {
        return (
            <View style={styles.emptyDate}>
                <Text>This is empty date!</Text>
            </View>
        );
    }

    const renderItemsByDate = (date) => {
        console.log("rendered day", date, productItems);

    }

    const onDayClicked = (day) => {
        console.log("day", day);
        navigation.navigate("Add Product", { day: day });
    }

    return (
        <View style={{ flex: 1 }}>
            <View style={styles.moduleDetail}>
                <Text style={styles.subHeader}>Click on any day to book items for that day</Text>
            </View>
            <Agenda
                // The list of items that have to be displayed in agenda. If you want to render item as empty date
                // the value of date key has to be an empty array []. If there exists no value for date key it is
                // considered that the date in question is not yet loaded
                items={items}
                // Callback that gets called when items for a certain month should be loaded (month became visible)
                loadItemsForMonth={month => {
                    console.log('trigger items loading', month.dateString)
                    renderItemsByDate(month.dateString);
                    loadItems(month);
                    // onDayClicked(month.dateString);
                }}
                renderItem={renderItem}
                onDayPress={day => {
                    console.log("day", day.dateString)
                    onDayClicked(day.dateString)
                }}
                renderEmptyDate={renderEmptyDate}

            />
        </View>
    )
}

export default Calender;

const styles = StyleSheet.create({
    calendarView: {
        justifyContent: "center",
        alignItem: "center"
    },
    moduleDetail: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "grey",
        // marginTop: 10,
        // marginBottom: 10,
        padding: 5
    },
    subHeader: {
        fontSize: 18,
        color: "black",
    },
    card: {
        backgroundColor: "white",
        width: "90%",
        // height: 150,
        borderRadius: 10,
        marginTop: 20,
    },
    image: {
        height: "60%",
        width: "100%",
        backgroundColor: "white",
        borderTopRightRadius: 10,
        borderTopLeftRadius: 10
    },
    cardText: {
        fontSize: 14,
        color: "black",
        marginLeft: 5,
        marginRight: 5,
        width: 100
    },
})