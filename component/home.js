import { Text, TouchableOpacity, StyleSheet, View,Dimensions, Image, FlatList } from "react-native";
import { useNavigation } from "@react-navigation/native";
import Modal from "react-native-modal";
import { useState, useEffect } from "react";
const width = Dimensions.get('screen').width;
const HomeScreen = () => {

    const navigation = useNavigation();

    return (
        <>
             <View style={styles.mainView}>
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("calender")}>
                        <Text style={{ color: "white" }}>Subscription</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.button} onPress={() => navigation.navigate("Show Product")}>
                        <Text style={{ color: "white" }}>Show Items</Text>
                    </TouchableOpacity>
            </View> 
        </>
    )
}

export default HomeScreen;

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "grey"
    },
    button: {
        height: 40,
        width: 100,
        backgroundColor: "green",
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 10,
        margin :20
    }
})