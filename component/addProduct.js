import { View, Text, Image, StyleSheet, TextInput, KeyboardAvoidingView, ScrollView, Alert, Platform } from 'react-native';
import { useEffect, useState } from 'react';
import * as React from 'react';
import { TouchableOpacity } from 'react-native';
import { useNavigation } from "@react-navigation/native";
import * as ImagePicker from 'react-native-image-picker';
import { openDatabase } from 'react-native-sqlite-storage';
var db = openDatabase({ name: 'UserDatabase.db' });

const AddProduct = ({ route }) => {

    const navigation = useNavigation();
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [description, setDescription] = useState('');
    const [uri, setUri] = useState('');

    useEffect(() => {
        db.transaction((txn) => {
            txn.executeSql(
                "SELECT name FROM sqlite_master WHERE type='table' AND name='product_table2'",
                [],
                (tx, res) => {
                    console.log('item:', res.rows.length);
                    if (res.rows.length == 0) {
                        txn.executeSql('DROP TABLE IF EXISTS product_table2', []);
                        txn.executeSql(
                            'CREATE TABLE IF NOT EXISTS product_table2(id INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(20), price INT(10), description VARCHAR(255), imageUri VARCHAR(255), date_column DATE)',
                            []
                        );
                    }
                }
            );
        });
    }, []);

    const uploadImage = () => {
        const options = {
            naData: true,
            rotation: 360,
            saveToPhotos: true,
            mediaType: 'photo',
            maxHeight: 800,
            maxWidth: 800,
        }
        const result = ImagePicker.launchImageLibrary(options, (response) => {
            console.log("response", response);
            if (response.didCancel == true) {
                Alert.alert("Loading Failed", " Try again to upload image")
            } else {
                response.assets.map((item) => setUri(item.uri));
            }

        });
        console.log("image", result);

    }

    const saveProduct = () => {
        console.log(name, price, description, route.params.day);
        if (!name) {
            alert('Please fill name');
            return;
        }
        if (!price) {
            alert('Please fill Contact Number');
            return;
        }
        if (!description) {
            alert('Please fill Address');
            return;
        }
        if (!uri) {
            alert('Please upload image');
            return;
        }

        db.transaction(function (tx) {
            tx.executeSql(
                'INSERT INTO product_table2 (name, price, description ,imageUri ,date_column) VALUES (?,?,?,?,?)',
                [name, price, description, uri, route.params.day],
                (tx, results) => {
                    console.log('Results', results);
                    if (results.rowsAffected > 0) {
                        Alert.alert("Success", "New product added successfully", [
                            {
                                text: 'Ok', onPress: () => navigation.navigate("Show Product")
                            }
                        ]);
                        setName('');
                        setPrice('');
                        setDescription('');
                        setUri('');
                    } else {
                        Alert.alert("failed")
                    }
                }
            );
        });
    }


    return (
        <ScrollView
            behavior={Platform.OS == "ios" ? "padding" : "height"}
            style={{ flex: 1, backgroundColor: "grey" }}
            contentContainerStyle={{ justifyContent: "center", alignItems: "center" }}
        >
            <KeyboardAvoidingView
                style={{ width: "100%", height: "100%" }}
                behavior="position"
            >
                <View style={styles.mainView}>
                    <View style={{ width: "80%", marginTop: 20, justifyContent: "center", alignItems: "center" }}>
                        <Text style={[styles.textInputHeader, { fontSize: 18 }]}> Date : {route.params.day}</Text>
                    </View>
                    <View style={{ width: "80%", marginBottom: 5, marginTop: 30 }}>
                        <Text style={styles.textInputHeader}>Name</Text>
                    </View>

                    <View style={styles.inputRow}>
                        <TextInput
                            onChangeText={(entered) => setName(entered)}
                            value={name}
                            placeholder=" Product name"
                            keyboardType="default"
                        />
                    </View>
                    <View style={{ width: "80%", marginBottom: 5 }}>
                        <Text style={styles.textInputHeader}>Price</Text>
                    </View>
                    <View style={styles.inputRow}>
                        <TextInput
                            onChangeText={(entered) => setPrice(entered)}
                            value={price}
                            placeholder=" Product price"
                            keyboardType="numeric"
                        />
                    </View>
                    <View style={{ width: "80%", marginBottom: 5 }}>
                        <Text style={styles.textInputHeader}>Description</Text>
                    </View>
                    <View style={[styles.inputRow, { height: 120 }]}>
                        <TextInput
                            onChangeText={(entered) => setDescription(entered)}
                            value={description}
                            placeholder="Write about product....."
                            keyboardType="default"
                            multiline={true}
                        />
                    </View>
                    <View style={[styles.inputRow, { height: 120, justifyContent: "center", alignItems: "center", marginTop: 10 }]}>
                        {uri ? <Image
                            resizeMode='cover'
                            style={{ height: "100%", width: "80%" }}
                            source={{ uri: uri }}
                        /> : <Text> Image not Available</Text>
                        }
                    </View>


                    <TouchableOpacity style={[styles.inputRow, { marginTop: 20, backgroundColor: "green", justifyContent: "center", alignItems: "center" }]}
                        onPress={() => uri ? saveProduct() : uploadImage()}
                    >
                        <Text style={styles.textInputHeader}>{uri ? "Save Product" : "Select Image"}</Text>
                    </TouchableOpacity>
                </View>
            </KeyboardAvoidingView>
        </ScrollView>
    )
}

export default AddProduct;

const styles = StyleSheet.create({
    mainView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        // marginTop:30
    },
    inputRow: {
        height: 50,
        width: "80%",
        backgroundColor: "white",
        borderRadius: 10,
        marginBottom: 10,
        // justifyContent: "center",
        // alignItems: "center",
    },
    textInputHeader: {
        fontSize: 16,
        color: "black"
    }
})